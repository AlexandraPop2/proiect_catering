package presentationLayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class LogInFrame extends JFrame {
    private JTextField username_a = new JTextField("Username");
    private JTextField password_a = new JTextField("Password");
    private JTextField username_r = new JTextField("Username");
    private JTextField password_r = new JTextField("Password");
    private JButton logIn = new JButton("Log in");
    private JButton register = new JButton("Register");
    private JButton authentication = new JButton("Authentication");
    private JButton registration = new JButton("Registration");
    private JButton back_a = new JButton("Back");
    private JButton back_r = new JButton("Back");

    private JPanel aPanel = new JPanel();
    private JPanel rPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private Administrator aP = new Administrator();
    private Client cP = new Client();
    private Employee eP = new Employee();


    public LogInFrame(){
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setPreferredSize(new Dimension(600, 300));
        mainPanel.add(Box.createRigidArea(new Dimension(150, 100)));
        mainPanel.add(authentication);
        mainPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        mainPanel.add(registration);

        username_a.setPreferredSize(new Dimension(50, 20));
        password_a.setPreferredSize(new Dimension(50, 20));

        username_r.setPreferredSize(new Dimension(50, 20));
        password_r.setPreferredSize(new Dimension(50, 20));

        setAPanel();
        setRPanel();


        this.setContentPane(mainPanel);
        this.pack();
        this.setTitle("Catering System");
        this.setPreferredSize(new Dimension(600, 300));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void setAPanel(){
        JPanel pFill1 =  new JPanel();
        pFill1.setPreferredSize(new Dimension(150, 500));
        JPanel pFill2 = new JPanel();

        pFill2.setPreferredSize(new Dimension(150, 500));
        JPanel pMain = new JPanel();

        pMain.setPreferredSize(new Dimension(200, 500));
        pMain.setLayout(new BoxLayout(pMain, BoxLayout.Y_AXIS));
        pMain.add(Box.createRigidArea(new Dimension(0, 50)));
        pMain.add(username_a);
        pMain.add(Box.createRigidArea(new Dimension(100, 20)));
        pMain.add(password_a);
        pMain.add(Box.createRigidArea(new Dimension(100, 50)));
        pMain.add(logIn);
        pMain.add(back_a);
        pMain.add(Box.createRigidArea(new Dimension(0, 50)));

        aPanel.setLayout(new BoxLayout(aPanel, BoxLayout.X_AXIS));
        aPanel.setPreferredSize(new Dimension(500, 500));
        aPanel.add(pFill1);
        aPanel.add(pMain);
        aPanel.add(pFill2);
    }

    private void setRPanel(){
        JPanel pFill1 =  new JPanel();
        pFill1.setPreferredSize(new Dimension(150, 500));
        JPanel pFill2 = new JPanel();

        pFill2.setPreferredSize(new Dimension(150, 500));
        JPanel pMain = new JPanel();

        pMain.setPreferredSize(new Dimension(200, 500));
        pMain.setLayout(new BoxLayout(pMain, BoxLayout.Y_AXIS));
        pMain.add(Box.createRigidArea(new Dimension(0, 50)));
        pMain.add(username_r);
        pMain.add(Box.createRigidArea(new Dimension(100, 20)));
        pMain.add(password_r);
        pMain.add(Box.createRigidArea(new Dimension(100, 50)));
        pMain.add(register);
        pMain.add(back_r);
        pMain.add(Box.createRigidArea(new Dimension(0, 50)));

        rPanel.setLayout(new BoxLayout(rPanel, BoxLayout.X_AXIS));
        rPanel.setPreferredSize(new Dimension(500, 500));
        rPanel.add(pFill1);
        rPanel.add(pMain);
        rPanel.add(pFill2);
    }

    public void changePanel(int i){
        if(i == 0){  this.setContentPane(rPanel); this.revalidate(); this.repaint(); }
        else if(i == 1){ this.setContentPane(aPanel); this.revalidate(); this.repaint(); }
        else if(i == 2){ this.setContentPane(aP); this.revalidate(); this.repaint(); }
        else if(i == 3){ this.setContentPane(cP); this.revalidate(); this.repaint(); }
        else if(i == 4){
            //this.setContentPane(eP); this.revalidate(); this.repaint();
            eP.setVisible(true);
        }
        else if(i == 5){ this.setContentPane(mainPanel); this.revalidate(); this.repaint(); }
        else if(i == 6){ this.setContentPane(cP.getViewPanel()); this.revalidate(); this.repaint(); }
        else if(i == 7){ this.setContentPane(cP.getOrderPanel()); this.revalidate(); this.repaint(); }
        else if(i == 8){ this.setContentPane(aP.getReportCriteria()); this.revalidate(); this.repaint(); }
        else if(i == 9){ this.setContentPane(aP.getCreateCompositeProductPanel()); this.revalidate(); this.repaint(); }
    }

    public Administrator getAP(){
        return aP;
    }

    public Client getCP(){
        return cP;
    }

    public Employee getEP(){
        return eP;
    }

    public String getUsernameA(){
        return username_a.getText();
    }

    public String getPasswordA(){
        return password_a.getText();
    }

    public String getUsernameR(){
        return username_r.getText();
    }

    public String getPasswordR(){
        return password_r.getText();
    }

    public void addAuthenticationButtonListener(ActionListener e){
        authentication.addActionListener(e);
    }

    public void addRegistrationButtonListener(ActionListener e){
        registration.addActionListener(e);
    }

    public void addLogInButtonListener(ActionListener e){
        logIn.addActionListener(e);
    }

    public void addRegisterButtonListener(ActionListener e){
        register.addActionListener(e);
    }

    public void addBackButtonListener(ActionListener e){
        back_a.addActionListener(e);
        back_r.addActionListener(e);
    }

}
