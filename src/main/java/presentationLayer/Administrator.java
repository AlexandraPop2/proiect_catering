package presentationLayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Administrator extends JPanel {
    //pagina principala a admin-ului:
    private JButton importProducts = new JButton("Import");
    private JTextField fileName = new JTextField("Filename");
    private JButton addButton = new JButton("Add");
    private JTextField titleToDelete = new JTextField("Title");
    private JButton deleteProduct = new JButton("Delete");

    private JTextField modifyTitle = new JTextField("Title");
    private JTextField modifyCalories = new JTextField("Calories");
    private JTextField modifyProtein = new JTextField("Protein");
    private JTextField modifyFat = new JTextField("Fat");
    private JTextField modifySodium = new JTextField("Sodium");
    private JTextField modifyPrice = new JTextField("Price");
    private JButton modifyProduct = new JButton("Modify");

    private JTextField compositeTitle = new JTextField("Title");
    private JButton createProduct = new JButton("Create");
    //pagina de create composite product:
    private JPanel createCompositeProducts = new JPanel();
    private JButton addCompositeProduct = new JButton("Create Product");
    private JButton backBtnCompositeProduct = new JButton("Back");

    private JButton generateReport = new JButton("Generate");
    private JButton mainBackButton = new JButton("Back");

    private JPanel addProductPanel = new JPanel();
    private JTextField title = new JTextField("title");
    private JTextField rating = new JTextField("rating");
    private JTextField calories = new JTextField("calories");
    private JTextField fat = new JTextField("fat");
    private JTextField protein = new JTextField("protein");
    private JTextField sodium = new JTextField("sodium");
    private JTextField price = new JTextField("price");
    private JButton addProduct = new JButton("Add Product");
    private JButton backBtn = new JButton("Back");


    private JPanel reportCriteria = new JPanel();

    private JButton timeInterval = new JButton("Time interval");
    private JTextField startTime = new JTextField("Start Time");
    private JTextField endTime = new JTextField("End Time");

    private JButton specifiedTimes = new JButton("Specified times");
    private JTextField noTimes = new JTextField("noTimes");

    private JButton specifiedTimesPerClient = new JButton("Specified times per Client");
    private JTextField noTimesPerClient = new JTextField("noTimesPerClient");
    private JTextField specifiedAmount = new JTextField("Amount");

    private JButton specifiedDate = new JButton("Specified date");
    private JTextField date = new JTextField("date");

    private JButton backButtonGenerate = new JButton("Back");

    private JTable jt;

    public Administrator(){
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JPanel importPanel = new JPanel(); importPanel.setLayout(new BoxLayout(importPanel, BoxLayout.X_AXIS));
        importPanel.setPreferredSize(new Dimension(500, 10)); importPanel.add(fileName); importPanel.add(importProducts);
        this.add(Box.createRigidArea(new Dimension(0, 25))); this.add(importPanel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        JPanel tempPanel = new JPanel(); tempPanel.setLayout(new BoxLayout(tempPanel, BoxLayout.X_AXIS));
        tempPanel.setPreferredSize(new Dimension(500, 10)); tempPanel.add(titleToDelete); tempPanel.add(deleteProduct);
        this.add(tempPanel); this.add(Box.createRigidArea(new Dimension(0, 10)));

        JPanel modifyPanel = new JPanel(); modifyPanel.setLayout(new BoxLayout(modifyPanel, BoxLayout.X_AXIS));
        modifyPanel.setPreferredSize(new Dimension(500, 10));
        modifyPanel.add(modifyTitle); modifyPanel.add(modifyCalories); modifyPanel.add(modifyProtein); modifyPanel.add(modifySodium);
        modifyPanel.add(modifyFat); modifyPanel.add(modifyPrice); modifyPanel.add(modifyProduct); this.add(modifyPanel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        JPanel createPanel = new JPanel(); createPanel.setLayout(new BoxLayout(createPanel, BoxLayout.X_AXIS));
        createPanel.setPreferredSize(new Dimension(500, 10));
        JScrollPane sp = new JScrollPane(jt); createPanel.add(compositeTitle); createPanel.add(createProduct); this.add(createPanel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        this.add(addButton); this.add(generateReport); this.add(mainBackButton);

        createCompositeProducts.setLayout(new BoxLayout(createCompositeProducts, BoxLayout.Y_AXIS)); createAddPoductPanel();

        reportCriteria.setLayout(new BoxLayout(reportCriteria, BoxLayout.Y_AXIS)); reportCriteria.add(timeInterval);
        reportCriteria.add(startTime); reportCriteria.add(endTime); reportCriteria.add(specifiedTimes);
        reportCriteria.add(noTimes); reportCriteria.add(specifiedTimesPerClient); reportCriteria.add(noTimesPerClient);
        reportCriteria.add(specifiedAmount); reportCriteria.add(specifiedDate); reportCriteria.add(date); reportCriteria.add(backButtonGenerate);
    }

    public void createAddPoductPanel(){
        addProductPanel.setLayout(new BoxLayout(addProductPanel, BoxLayout.Y_AXIS));
        addProductPanel.setPreferredSize(new Dimension(500, 500));
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 50)));
        addProductPanel.add(title);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(rating);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(calories);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(fat);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(sodium);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(protein);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(price);
        addProductPanel.add(Box.createRigidArea(new Dimension(150, 0)));
        addProductPanel.add(addProduct);
        addProductPanel.add(backBtn);
    }

    public void makePanel(Vector data, Vector column){
        createCompositeProducts.removeAll();
        jt = new JTable(data, column);
        JScrollPane sp = new JScrollPane(jt);
        createCompositeProducts.add(sp);
        createCompositeProducts.add(Box.createRigidArea(new Dimension(150, 0)));
        createCompositeProducts.add(addCompositeProduct);
        createCompositeProducts.add(Box.createRigidArea(new Dimension(150, 0)));
        createCompositeProducts.add(backBtnCompositeProduct);
    }

    public String getFileName(){
        return fileName.getText();
    }

    public JPanel getAddProductPanel(){
        return addProductPanel;
    }

    public JPanel getReportCriteria(){ return reportCriteria; }

    public String getTitle(){
        return title.getText();
    }

    public String getCalories(){ return calories.getText(); }

    public String getRating(){ return rating.getText(); }

    public String getProtein(){ return protein.getText(); }

    public String getFat(){ return fat.getText(); }

    public String getSodium(){ return sodium.getText(); }

    public String getPrice(){ return price.getText(); }

    public String getStartTime(){ return startTime.getText(); }

    public String getEndTime(){ return endTime.getText(); }

    public String getNoTimes(){ return noTimes.getText(); }

    public String getNoTimesPerClient(){ return noTimesPerClient.getText(); }

    public String getAmount(){ return specifiedAmount.getText(); }

    public String getDate(){ return date.getText(); }

    public String getTitleToDelete(){ return titleToDelete.getText(); }

    public String getModifyTitle(){ return modifyTitle.getText(); }

    public String getModifyCalories() { return modifyCalories.getText(); }

    public String getModifyProtein(){ return modifyProtein.getText(); }

    public String getModifyFat(){ return modifyFat.getText(); }

    public String getModifySodium(){ return modifySodium.getText(); }

    public String getModifyPrice(){ return modifyPrice.getText(); }

    public String getCompositeTitle(){ return compositeTitle.getText(); }

    public JPanel getCreateCompositeProductPanel(){ return createCompositeProducts; }

    public JTable getJt(){ return jt; }

    public void addImportButtonListener(ActionListener e){
        importProducts.addActionListener(e);
    }

    public void addAddButtonListener(ActionListener e){
        addButton.addActionListener(e);
    }

    public void addModifyButtonListener(ActionListener e){
        modifyProduct.addActionListener(e);
    }

    public void addDeleteButtonListener(ActionListener e){
        deleteProduct.addActionListener(e);
    }

    public void addCreateButtonListener(ActionListener e){
        createProduct.addActionListener(e);
    }

    public void addGenerateButtonListener(ActionListener e){
        generateReport.addActionListener(e);
    }

    public void addAddProductButtonListener(ActionListener e){
        addProduct.addActionListener(e);
    }

    public void addBackBtnListener(ActionListener e){ backBtn.addActionListener(e); }

    public void addMainBackBtnListener(ActionListener e){ mainBackButton.addActionListener(e); }

    public void addTimeIntervalListener(ActionListener e){ timeInterval.addActionListener(e); }

    public void addSpecifiedTimesListener(ActionListener e){ specifiedTimes.addActionListener(e); }

    public void addSpecifiedTimesPerClient(ActionListener e){ specifiedTimesPerClient.addActionListener(e); }

    public void addBackGenerateBtnListener(ActionListener e){ backButtonGenerate.addActionListener(e); }

    public void addDateGenerateBtnListener(ActionListener e){ specifiedDate.addActionListener(e); }

    public void addCreateCompositeProductListener(ActionListener e){ addCompositeProduct.addActionListener(e); }

    public void addBackBtnCreateListener(ActionListener e){ backBtnCompositeProduct.addActionListener(e); }
}
