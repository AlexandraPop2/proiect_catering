package presentationLayer;

import businessLayer.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.stream.Collectors;

public class UIController {
    private LogInFrame lF; private Accounts ac; private DeliveryService deliveryModel;

    public UIController(LogInFrame view, Accounts ac, DeliveryService deliveryModel){
        lF = view; this.ac = ac; this.deliveryModel = deliveryModel; deliveryModel.addObserver(lF.getEP());

        lF.addAuthenticationButtonListener(new AuthenticationListener());
        lF.addRegistrationButtonListener(new RegistrationListener());
        lF.addLogInButtonListener(new LogInListener());
        lF.addRegisterButtonListener(new RegisterListener());
        lF.addBackButtonListener(new BackButtonListener());
        lF.getCP().addViewProductsListener(new ViewProductsListener());
        lF.getCP().addSearchProductsListener(new SearchProductsListener());
        lF.getCP().addMakeOrderListener(new MakeOrderListener());
        lF.getCP().addBackButtonListener(new CPBackButtonListener());
        lF.getCP().addMakeOrderBtnListener(new MakeOrderBtnListener());
        lF.getCP().addMainBackBtnListener(new BackButtonListener());
        lF.getEP().addBackBtnListener(new BackButtonListener());
    }

    class AuthenticationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            lF.changePanel(1);
        }
    }

    class RegistrationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            lF.changePanel(0);
        }
    }

    class LogInListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String username = lF.getUsernameA();
            String password = lF.getPasswordA();

            if(username.equals("Admin") && password.equals("PassAdmin")){ lF.changePanel(2); }
            else if(username.equals("RegularEm") && password.equals("PassREm")){ lF.changePanel(4); }
            else if(ac.accountMatching(username, password)){ lF.changePanel(3); }
            else{ JOptionPane.showMessageDialog(lF, "Username and password not matching or the account does not exist!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
        }
    }

    class RegisterListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String username = lF.getUsernameR();
            String password = lF.getPasswordR();

            if(!ac.accountMatching(username, password)){
                ac.addAccount(username, password, ac.getNbOfAccount() + 1);
                JOptionPane.showMessageDialog(lF, "Created successfully!", "Success Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{ JOptionPane.showMessageDialog(lF, "Account already exists!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
        }
    }

    class BackButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) { lF.changePanel(5); lF.getEP().setVisible(false); }
    }

    class ViewProductsListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Vector data = deliveryModel.viewProducts(deliveryModel.getMenuList());
            Vector column = new Vector();
            column.add("Title"); column.add("Contents"); column.add("Calories"); column.add("Protein"); column.add("Fat");
            column.add("Sodium"); column.add("Price"); column.add("Ratings");
            lF.getCP().viewProducts(data, column);
            lF.changePanel(6);
        }
    }

    class SearchProductsListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Vector data = new Vector(); String title = lF.getCP().getTitle();
            int calories = -1; int protein = -1; int fat = -1; int sodium = -1; double rating = -1.0; int price = -1;

            try{ calories = Integer.parseInt(lF.getCP().getCalories()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Caloriile nu sunt transmise corect, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            try{ protein = Integer.parseInt(lF.getCP().getProtein()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Proteinele nu sunt transmise corect, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            try{ fat = Integer.parseInt(lF.getCP().getFat()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Grasimile nu sunt transmise corect, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            try{ sodium = Integer.parseInt(lF.getCP().getSodium()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Sodiul nu este transmis corect, nu va fi luat in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            try{ rating = Double.parseDouble(lF.getCP().getRating()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Ratingul nu este transmis corect, nu va fi luat in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            try{ price = Integer.parseInt(lF.getCP().getPrice()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Pretul nu este transmis corect, nu va fi luat in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            List criteria = new ArrayList();criteria.add(title); criteria.add(calories); criteria.add(rating);
            criteria.add(fat); criteria.add(sodium); criteria.add(protein); criteria.add(price);

            List<MenuItem> searchedProducts = deliveryModel.searchForProduct(criteria);
            data = deliveryModel.viewProducts(searchedProducts);

            Vector column = new Vector(); column.add("Title"); column.add("Contents"); column.add("Calories");
            column.add("Protein"); column.add("Fat"); column.add("Sodium"); column.add("Price"); column.add("Ratings");

            lF.getCP().viewProducts(data, column); lF.changePanel(6);
        }
    }

    class MakeOrderListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Vector data = new Vector();
            for(MenuItem m: deliveryModel.getMenuList()){
                Vector v = new Vector(); v.add(m.getTitle());
                if(m instanceof CompositeProduct){
                    List<String> items = ((CompositeProduct) m).getList().stream().map(s -> new String(s.getTitle())).collect(Collectors.toList());
                    v.add(items);
                }
                else{ v.add("No components"); }

                v.add(m.computeCalories()); v.add(m.computeProtein());
                v.add(m.computeFat()); v.add(m.computeSodium());
                v.add(m.computePrice()); v.add(m.computeRatings());
                v.add(Boolean.FALSE); data.add(v);
            }

            Vector column = new Vector();
            column.add("Title"); column.add("Contents"); column.add("Calories"); column.add("Protein"); column.add("Fat");
            column.add("Sodium"); column.add("Price"); column.add("Ratings"); column.add("SELECTED");

            lF.getCP().makeOrderPanel(data, column);
            lF.changePanel(7);
        }
    }

    class CPBackButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            lF.changePanel(3);
        }
    }

    class MakeOrderBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            List<MenuItem> orderObjects = new ArrayList<MenuItem>();
            int count = lF.getCP().getJt().getRowCount();
            for (int i = 0; i < count; i++) {
                if (lF.getCP().getJt().getValueAt(i, 8).toString().equals("true")) {
                    orderObjects.add(deliveryModel.getMenuList().get(i));
                }
            }
            if (deliveryModel.createNewOrder(orderObjects, ac.getAccountId(lF.getUsernameA()))) {
                JOptionPane.showMessageDialog(lF, "Comanda adaugata!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Alegeti cel putin un produs pentru a putea plasa comanda!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }

            deliveryModel.getListOfOrders().keySet().stream().forEach(System.out::println);
            deliveryModel.getListOfOrders().values().stream().forEach(System.out::println);
        }
    }
}
