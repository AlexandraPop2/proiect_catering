package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.DeliveryService;
import businessLayer.MenuItem;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AdminController {
    private LogInFrame lF;
    private DeliveryService del;

    public AdminController(DeliveryService del, LogInFrame lF){
        this.lF = lF;
        this.del = del;

        lF.getAP().addImportButtonListener(new ImportButtonListener());
        lF.getAP().addAddButtonListener(new AddButtonListener());
        lF.getAP().addDeleteButtonListener(new DeleteButtonListener());
        lF.getAP().addModifyButtonListener(new ModifyButtonListener());
        lF.getAP().addCreateButtonListener(new CreateButtonListener());
        lF.getAP().addGenerateButtonListener(new GenerateButtonListener());
        lF.getAP().addAddProductButtonListener(new AddProductListener());
        lF.getAP().addCreateCompositeProductListener(new AddCompositeProductListener());
        lF.getAP().addTimeIntervalListener(new TimeIntervalListener());
        lF.getAP().addSpecifiedTimesListener(new SpecifiedTimesListener());
        lF.getAP().addSpecifiedTimesPerClient(new SpecifiedTimesPerClient());
        lF.getAP().addDateGenerateBtnListener(new DateBtnListener());
        lF.getAP().addBackGenerateBtnListener(new APBackButtonListener());
        lF.getAP().addBackBtnCreateListener(new APBackButtonListener());
        lF.getAP().addMainBackBtnListener(new BackButtonListener());
        lF.getAP().addBackBtnListener(new APBackButtonListener());
    }

    class BackButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) { lF.changePanel(5); lF.getEP().setVisible(false); }
    }

    class APBackButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) { lF.changePanel(2); }
    }

    class ImportButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String filename = lF.getAP().getFileName();
            if(del.importItems(filename)){ JOptionPane.showMessageDialog(lF, "Produse importate cu succes!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            else{ JOptionPane.showMessageDialog(lF, "Fisierul nu exista!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
        }
    }

    class AddButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            lF.setContentPane(lF.getAP().getAddProductPanel()); lF.revalidate(); lF.repaint();
        }
    }

    class ModifyButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String title = lF.getAP().getModifyTitle();
            int calories = -1; int protein = -1; int fat = -1; int sodium = -1; int price = -1;

            try{ calories = Integer.parseInt(lF.getAP().getModifyCalories()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Caloriile sunt invalide, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            try{ fat = Integer.parseInt(lF.getAP().getModifyFat()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Grasimile sunt invalide, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            try{ protein = Integer.parseInt(lF.getAP().getModifyProtein()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Proteinele sunt invalide, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            try{ sodium = Integer.parseInt(lF.getAP().getModifySodium()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Sodiul sunt invalide, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            try{ price = Integer.parseInt(lF.getAP().getModifyPrice()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "Pretul sunt invalide, nu vor fi luate in considerare!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            if(del.modifyProduct(title, calories, fat, sodium, price, protein) == true){
                JOptionPane.showMessageDialog(lF, "Modificat cu succes!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Produsul nu exista!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class DeleteButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String title = lF.getAP().getTitleToDelete();
            if(del.deleteProduct(title) == true){
                JOptionPane.showMessageDialog(lF, "Produsul a fost sters!!", "Success Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Nu s-a putut sterge produsul!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class CreateButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Vector data = new Vector();
            for(MenuItem m: del.getMenuList()){
                Vector v = new Vector(); v.add(m.getTitle());
                if(m instanceof CompositeProduct){
                    List<String> items = ((CompositeProduct) m).getList().stream().map(s -> new String(s.getTitle())).collect(Collectors.toList());
                    v.add(items);
                }
                else{ v.add("No components"); }

                v.add(m.computeCalories()); v.add(m.computeProtein());
                v.add(m.computeFat()); v.add(m.computeSodium());
                v.add(m.computePrice()); v.add(m.computeRatings());
                v.add(Boolean.FALSE); data.add(v);
            }
            Vector column = new Vector();
            column.add("Title"); column.add("Contents"); column.add("Calories"); column.add("Protein"); column.add("Fat");
            column.add("Sodium"); column.add("Price"); column.add("Ratings"); column.add("SELECTED");
            lF.getAP().makePanel(data, column);
            lF.changePanel(9);
        }
    }

    class AddCompositeProductListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String title = lF.getAP().getCompositeTitle();
            List<BaseProduct> createObjects = new ArrayList<BaseProduct>();
            int count = lF.getAP().getJt().getRowCount();
            for (int i = 0; i < count; i++) {
                if (lF.getAP().getJt().getValueAt(i, 8).toString().equals("true") && del.getMenuList().get(i) instanceof BaseProduct) {
                    createObjects.add((BaseProduct) del.getMenuList().get(i));
                }
            }

            if (createObjects.size() != 0 && del.createCompositeProduct(createObjects, title)) {
                JOptionPane.showMessageDialog(lF, "Produs nou adaugat!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Alegeti cel putin un produs pentru a putea crea un produs compus!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class GenerateButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            lF.changePanel(8);
        }
    }

    class AddProductListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String title = lF.getAP().getTitle();
            double rating = 0.0; int calories = 0; int fat = 0; int sodium = 0; int protein = 0; int price = 0;
            boolean ok = true;

            if(title.equals("title")){
                JOptionPane.showMessageDialog(lF, "Setati titlul produsului!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
                ok = false;
            }
            try{
                rating = Double.parseDouble(lF.getAP().getRating()); calories = Integer.parseInt(lF.getAP().getCalories());
                fat = Integer.parseInt(lF.getAP().getFat()); sodium = Integer.parseInt(lF.getAP().getSodium());
                protein = Integer.parseInt(lF.getAP().getProtein()); price = Integer.parseInt(lF.getAP().getPrice());
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(lF, "Valorile nu sunt transmise corect!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
                ok = false;
            }
            if(ok == true && del.addProduct(title, rating, calories, fat, sodium, protein, price) == false){
                JOptionPane.showMessageDialog(lF, "Produsul exista deja!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class TimeIntervalListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String startTime = lF.getAP().getStartTime();
            String endTime = lF.getAP().getEndTime();
            Pattern p = Pattern.compile("[0-2][0-9]:[0-6][0-9]");
            Pattern splitP = Pattern.compile(":");
            int h1 = 0; int m1 = 0; int h2 = 0; int m2 = 0;
            LocalTime t1 = null; LocalTime t2 = null;

            if (p.matcher(startTime).matches() && p.matcher(endTime).matches()) {
                String[] sT = splitP.split(startTime); String[] sTE = splitP.split(endTime);
                h1 = Integer.parseInt(sT[0]); m1 = Integer.parseInt(sT[1]);
                h2 = Integer.parseInt(sTE[0]); m2 = Integer.parseInt(sTE[1]);
                t1 = LocalTime.of(h1, m1); t2 = LocalTime.of(h2, m2);
            }
            else {
                JOptionPane.showMessageDialog(lF, "Timpii nu au formatul bun - hh:mm!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            System.out.println(t1);
            System.out.println(t2);

            if (del.generateTimeIntervalReport(t1, t2) == false) {
                JOptionPane.showMessageDialog(lF, "Could not generate the report!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(lF, "Report generate successfully!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class SpecifiedTimesListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int noTimes = 0;
            try{ noTimes = Integer.parseInt(lF.getAP().getNoTimes()); }
            catch(NumberFormatException ex){ JOptionPane.showMessageDialog(lF, "No specified is invalid!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }

            if(noTimes != 0 && del.generateSpecifiedTimesReport(noTimes) != false){
                JOptionPane.showMessageDialog(lF, "Report generate successfully!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Could not generate the report!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class SpecifiedTimesPerClient implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int noTimesPerClient = 0; int amount = 0;
            try{
                noTimesPerClient = Integer.parseInt(lF.getAP().getNoTimesPerClient());
                amount = Integer.parseInt(lF.getAP().getAmount());
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(lF, "Invalid values!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }

            if(noTimesPerClient != 0 && amount != 0 && del.generateSpecifiedTimesPerClientReport(noTimesPerClient, amount) != false){
                JOptionPane.showMessageDialog(lF, "Report generate successfully!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Could not generate the report!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    class DateBtnListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String date = lF.getAP().getDate();
            LocalDate d = null;
            Pattern p = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");
            Pattern delimiter = Pattern.compile("-");
            if(p.matcher(date).matches()){
                String[] s = delimiter.split(date);
                try{ d = LocalDate.of(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2])); }
                catch (DateTimeException ex){ JOptionPane.showMessageDialog(lF, "Date format invalid!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE); }
            }
            else{
                JOptionPane.showMessageDialog(lF, "Date format invalid!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }

            if(d != null && del.generateSpecifiedDateReport(d)){
                JOptionPane.showMessageDialog(lF, "Report generated successfully!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(lF, "Couldn't generate report!!", "Warning Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }
}
