package presentationLayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class Employee extends JFrame implements Observer{
    private JButton backBtn = new JButton("Back");
    private JTextArea message = new JTextArea("Orders");

    public Employee(){
        JPanel mainPanel = new JPanel();
        mainPanel.setPreferredSize(new Dimension(600, 300));
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        JScrollPane sp = new JScrollPane(message);
        mainPanel.add(sp);
        mainPanel.add(backBtn);
        this.setContentPane(mainPanel);
        this.pack();
        this.setTitle("Employee Page");
        this.setPreferredSize(new Dimension(600, 300));
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof String){
            message.setText((String)arg);
        }
        else{
            JOptionPane.showMessageDialog(this, "No new order!", "Warning Message!", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void addBackBtnListener(ActionListener e){
        backBtn.addActionListener(e);
    }
}
