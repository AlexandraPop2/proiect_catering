package presentationLayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Client extends JPanel {
    private JButton viewProducts = new JButton("View Products");
    private JButton backBtn = new JButton("Back");

    private JButton mainBackBtn = new JButton("Back");

    private JTextField title = new JTextField("Title");
    private JTextField calories = new JTextField("Calories");
    private JTextField protein = new JTextField("Protein");
    private JTextField sodium = new JTextField("Sodium");
    private JTextField fat = new JTextField("Fat");
    private JTextField rating = new JTextField("Rating");
    private JTextField price = new JTextField("Price");
    private JButton searchForProducts = new JButton("Search");

    private JButton makeOrder = new JButton("Create Order");
    private JPanel orderPanel = new JPanel();
    private JButton makeOrderBtn = new JButton("Place Order");
    private JTable jt;

    private JPanel viewPanel = new JPanel();

    public Client(){
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel tempPanel = new JPanel();
        tempPanel.setLayout(new BoxLayout(tempPanel, BoxLayout.X_AXIS));
        tempPanel.add(title);
        tempPanel.add(calories);
        tempPanel.add(protein);
        tempPanel.add(sodium);
        tempPanel.add(fat);
        tempPanel.add(rating);
        tempPanel.add(price);
        tempPanel.add(searchForProducts);

        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(viewProducts);
        this.add(Box.createRigidArea(new Dimension(0, 100)));
        this.add(tempPanel);
        this.add(Box.createRigidArea(new Dimension(0, 80)));
        this.add(makeOrder);
        this.add(mainBackBtn);
        this.add(Box.createRigidArea(new Dimension(0, 50)));
    }

    public void viewProducts(Vector data, Vector column){
        viewPanel.removeAll();
        viewPanel.setLayout(new BoxLayout(viewPanel, BoxLayout.X_AXIS));

        JTable jt = new JTable(data, column);
        JScrollPane sp = new JScrollPane(jt);
        viewPanel.add(sp);
        viewPanel.add(backBtn);
    }

    public void makeOrderPanel(Vector data, Vector column){
        orderPanel.removeAll();
        orderPanel.setLayout(new BoxLayout(orderPanel, BoxLayout.X_AXIS));

        JPanel pFill = new JPanel();
        pFill.setLayout(new BoxLayout(pFill, BoxLayout.Y_AXIS));
        pFill.add(backBtn);
        pFill.add(makeOrderBtn);

        jt = new JTable(data, column);
        jt.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jt.setRowSelectionAllowed(true);
        JScrollPane sp = new JScrollPane(jt);
        orderPanel.add(sp);
        orderPanel.add(pFill);
    }

    public String getTitle(){ return title.getText(); }
    public String getCalories(){ return calories.getText(); }
    public String getProtein(){ return protein.getText(); }
    public String getSodium(){ return sodium.getText(); }
    public String getFat(){ return fat.getText(); }
    public String getRating(){ return rating.getText(); }
    public String getPrice(){ return price.getText(); }

    public JPanel getViewPanel(){ return viewPanel; }
    public JPanel getOrderPanel(){ return orderPanel; }
    public JTable getJt() { return jt; }

    public void addViewProductsListener(ActionListener e){
        viewProducts.addActionListener(e);
    }

    public void addSearchProductsListener(ActionListener e){
        searchForProducts.addActionListener(e);
    }

    public void addMakeOrderListener(ActionListener e){ makeOrder.addActionListener(e); }

    public void addBackButtonListener(ActionListener e){
        backBtn.addActionListener(e);
    }

    public void addMakeOrderBtnListener(ActionListener e){
        makeOrderBtn.addActionListener(e);
    }

    public void addMainBackBtnListener(ActionListener e){ mainBackBtn.addActionListener(e); }

}
