package dataLayer;

import businessLayer.MenuItem;
import businessLayer.Order;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FileWriterClass {

    public static java.io.FileWriter createFileWriter(){
        FileWriter fW = null;
        try{ fW = new FileWriter("Report.txt", false); }
        catch(IOException ex){ System.out.println("Nu se poate crea report!");}
        return fW;
    }

    public static boolean writeToFile(FileWriter fW, Map<Order, List<MenuItem>> lOrdersAndItems) {
        for(Map.Entry<Order, List<MenuItem>> m: lOrdersAndItems.entrySet()){
            try{ fW.write(m.getKey().toString()+"\nContents:\n");
                for(MenuItem e: m.getValue()){
                    fW.write(e.toString()+"\n");
                }
            } catch(IOException ex){ System.out.println("Nu se poate scrie in report!"); }
        }
        try{ fW.close(); }catch(IOException ex){ System.out.println("Nu se poate scrie in report!"); return false;}
        return true;
    }

    public static boolean writeProductsToFile(List<MenuItem> products, FileWriter fW){
        for(MenuItem m: products.stream().distinct().collect(Collectors.toList())){
            long noOfTimes = products.stream().filter(s -> s.equals(m)).count();
            try{ fW.write("Produs: "+m.getTitle().toString()+". Comandat de "+String.valueOf(noOfTimes)+" ori\n");
            } catch(IOException ex){ System.out.println("Nu se poate scrie in report!"); }
        }
        try{ fW.close(); }catch(IOException ex){ System.out.println("Nu se poate scrie in report!"); return false;}
        return true;
    }

    public static FileWriter createOrderFileWriter(Order newOrder){
        FileWriter fW = null;
        try{ fW = new FileWriter("bill"+String.valueOf(newOrder.getOrderID())+".txt", true); }
        catch(IOException ex){ System.out.println("Nu se poate crea chitanta!"); }
        return fW;
    }

    public static void writeToOrderFile(List<MenuItem> l, FileWriter fW){
        int finalPrice = 0;
        for(MenuItem m: l){
            finalPrice += m.computePrice();
            try{ fW.write(m.toString()+"\n"); }
            catch(IOException ex){ System.out.println("Nu se poate scrie in chitanta!"); }
        }
        try{ fW.write("Pret total: "+finalPrice+"\n");  fW.close(); }
        catch(IOException ex){ System.out.println("Nu se poate scrie in chitanta!"); }
    }
}
