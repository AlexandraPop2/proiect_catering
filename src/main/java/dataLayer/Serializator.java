package dataLayer;

import java.io.*;

public class Serializator implements Serializable{

    public void serialization(Object obj, String filename) {
        try{
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(obj);
            out.close();
            file.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public Object deserialization(String filename){
        Object o = new Object();
        try{
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);
            o = in.readObject();

            in.close();
            file.close();
        }
        catch (IOException | ClassNotFoundException e){
            System.out.println("Fisierul este gol!");
        }
        return o;
    }

}
