package businessLayer;

import dataLayer.FileWriterClass;
import dataLayer.Serializator;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @invariant isWellFormed()
 */

public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable{

    private List<MenuItem> menu = new ArrayList<MenuItem>();
    private Map<Order, List<MenuItem>> listOfOrders = new Hashtable<Order, List<MenuItem>>();

    protected boolean isWellFormed(){
        return menu != null && listOfOrders != null;
    }

    @Override
    public boolean importItems(String fileName) {
        assert menu.size() >= 0 : "Meniul este invalid!";
        assert isWellFormed();
        int oldSize = menu.size();

        BufferedReader r = null;
        try{ r = new BufferedReader(new FileReader(fileName)); }
        catch(FileNotFoundException e){}
        if(r == null){ return false; }

        List<BaseProduct> importedProducts = r.lines().skip(1).map(BaseProduct::createProduct).filter(s -> s != null).distinct().collect(Collectors.toList());

        menu.addAll(importedProducts);
        menu = menu.stream().distinct().collect(Collectors.toList());
        System.out.println(menu.size());

        Serializator ser = new Serializator();
        ser.serialization(this, "file.txt");

        assert menu.size() >= oldSize : "Meniul nu s-a modificat corect";
        assert isWellFormed();
        return true;
    }

    @Override
    public boolean addProduct(String title, double rating, int calories, int fat, int sodium, int protein, int price) {
        assert isWellFormed();
        int oldSize = menu.size();

        boolean ok = menu.stream().anyMatch(m -> m.getTitle().trim().equals(title.trim()));

        if(ok == false){
            BaseProduct p = new BaseProduct(title, rating, calories, protein, fat, sodium, price);
            menu.add(p);
            Serializator ser = new Serializator();
            ser.serialization(this, "file.txt");
            assert menu.size() == oldSize + 1 : "Dimensiunea meniului e gresita!";
            return true;
        }
        assert menu.size() == oldSize : "Dimensiunea meniului e gresita!";
        assert isWellFormed();
        return false;
    }

    @Override
    public boolean deleteProduct(String title) {
        assert title != null : "Titlul este null!"; assert isWellFormed(); int oldSize = menu.size();

        List<MenuItem> itemsToDelete = menu.stream().filter(s -> s.getTitle().trim().equals(title.trim())).collect(Collectors.toList());
        itemsToDelete.stream().forEach(System.out::println);

        if(itemsToDelete.size() != 0) {
            menu.removeAll(itemsToDelete);
            Serializator ser = new Serializator();
            ser.serialization(this, "file.txt");
            assert menu.size() == oldSize - 1 : "Lista nu are cu un element mai putin!";
            return true;
        }
        assert menu.size() == oldSize : "Lista are cu un parametru mai putin desi nu s-a sters niciun produs!";
        assert isWellFormed();
        return false;
    }

    @Override
    public boolean modifyProduct(String title, int calories, int fat, int sodium, int price, int protein) {
        assert title != null : "Titlul este null!"; assert isWellFormed(); int oldSize = menu.size();

        List<BaseProduct> itemsToModify = menu.stream().filter(s -> s instanceof BaseProduct && s.getTitle().trim().equals(title.trim()))
               .limit(1).map(BaseProduct::menuItemToBaseProduct).collect(Collectors.toList());

        if(itemsToModify.size() != 1){ return false; }
        else{
            BaseProduct itemToModify = itemsToModify.get(0);
            if(calories != -1){ ((BaseProduct)(menu.get(menu.indexOf(itemToModify)))).setCalories(calories); }
            if(fat != -1){ ((BaseProduct)(menu.get(menu.indexOf(itemToModify)))).setFat(fat); }
            if(sodium != -1){ ((BaseProduct)(menu.get(menu.indexOf(itemToModify)))).setSodium(sodium); }
            if(price != -1){ ((BaseProduct)(menu.get(menu.indexOf(itemToModify)))).setPrice(price); }
            if(protein != -1){ ((BaseProduct)(menu.get(menu.indexOf(itemToModify)))).setProtein(protein); }
        }
        Serializator ser = new Serializator();
        ser.serialization(this, "file.txt");

        assert menu.size() == oldSize : "Dimensiunea meniului nu e potrivita!"; assert isWellFormed();
        return true;
    }

    @Override
    public boolean createCompositeProduct(List<BaseProduct> l, String title) {
        assert title != null && l != null : "Parametri de intrare sunt invalizi!"; assert isWellFormed();
        int oldSize = menu.size();

        CompositeProduct newProduct = new CompositeProduct();
        newProduct.setTitle(title);
        for(BaseProduct p: l){ newProduct.getList().add(p); }

        if(!menu.stream().anyMatch(s -> s.getTitle().trim().equals(title.trim()))){
            menu.add(newProduct);
            Serializator ser = new Serializator();
            ser.serialization(this, "file.txt");
            assert menu.size() == oldSize + 1 : "Nu s-a adaugat produsul, meniul are tot atatea elemente ca inainte!";
            assert isWellFormed();
            return true;
        }
        assert menu.size() == oldSize : "Meniul ar trebui sa aiba tot atatea elemente ca la inceputul metodei!";
        assert isWellFormed();
        return false;
    }

    @Override
    public Vector viewProducts(List<MenuItem> menuM) {
        assert menuM != null : "Parametrul de intrare este nepotrivit!";
        assert isWellFormed();

        Vector data = new Vector();
        for(MenuItem m: menuM){
            Vector v = new Vector();
            v.add(m.getTitle());
            if(m instanceof CompositeProduct){
                List<String> items = ((CompositeProduct) m).getList().stream().map(s -> new String(s.getTitle())).collect(Collectors.toList());
                v.add(items);
            }
            else{ v.add("No components"); }
            v.add(m.computeCalories());
            v.add(m.computeProtein());
            v.add(m.computeFat());
            v.add(m.computeSodium());
            v.add(m.computePrice());
            v.add(m.computeRatings());
            data.add(v);
        }
        assert data != null : "Parametrul de iesire este invalid!";
        assert isWellFormed();
        return data;
    }

    @Override
    public List<MenuItem> searchForProduct(List criteria) {
        assert criteria != null && criteria.size() == 7 : "Parametri de intrare invalizi!"; assert isWellFormed();

        String title = (String) criteria.get(0); int calories = (int) criteria.get(1); double rating = (double) criteria.get(2);
        int fat = (int) criteria.get(3); int sodium = (int) criteria.get(4); int protein = (int) criteria.get(5);
        int price = (int) criteria.get(6);
        List<MenuItem> temporaryList;

        temporaryList = menu.stream().filter(s -> s.getTitle().trim().toLowerCase(Locale.ROOT).contains(title.trim().toLowerCase(Locale.ROOT))).collect(Collectors.toList());
        if(calories != -1){
            temporaryList = !temporaryList.isEmpty() ? temporaryList.stream().filter(s -> s.computeCalories() == calories).collect(Collectors.toList()) : menu.stream().filter(s -> s.computeCalories() == calories).collect(Collectors.toList());
        }
        if(protein != -1){
            temporaryList = !temporaryList.isEmpty() ? temporaryList.stream().filter(s -> s.computeProtein() == protein).collect(Collectors.toList()) : menu.stream().filter(s -> s.computeProtein() == protein).collect(Collectors.toList());
        }
        if(fat != -1){
            temporaryList = !temporaryList.isEmpty() ? temporaryList.stream().filter(s -> s.computeFat() == fat).collect(Collectors.toList()) : menu.stream().filter(s -> s.computeFat() == fat).collect(Collectors.toList());
        }
        if(sodium != -1){
            temporaryList = !temporaryList.isEmpty() ? temporaryList.stream().filter(s -> s.computeSodium() == sodium).collect(Collectors.toList()) : menu.stream().filter(s -> s.computeSodium() == sodium).collect(Collectors.toList());
        }
        if(rating != -1.0){
            temporaryList = !temporaryList.isEmpty() ? temporaryList.stream().filter(s -> s.computeRatings() == rating).collect(Collectors.toList()) : menu.stream().filter(s -> s.computeRatings() == rating).collect(Collectors.toList());
        }
        if(price != -1){
            temporaryList = !temporaryList.isEmpty() ? temporaryList.stream().filter(s -> s.computePrice() == price).collect(Collectors.toList()) : menu.stream().filter(s -> s.computePrice() == price).collect(Collectors.toList());
        }

        assert temporaryList != null : "Parametrul de iesire este invalid!"; assert isWellFormed();
        return temporaryList;
    }

    public List<MenuItem> getMenuList(){
        return menu;
    }

    public Map<Order, List<MenuItem>> getListOfOrders(){
        return listOfOrders;
    }

    @Override
    public boolean createNewOrder(List<MenuItem> l, int idClient) {
        assert l != null : "Parametru de intrare invalid!"; assert isWellFormed(); int oldSize = listOfOrders.size();

        Order newOrder = new Order(listOfOrders.size() + 1, idClient, LocalDateTime.now());
        if(!l.isEmpty()){
            listOfOrders.put(newOrder, l);
            FileWriter fW = FileWriterClass.createOrderFileWriter(newOrder);
            FileWriterClass.writeToOrderFile(l, fW);

            setChanged();
            notifyObservers(new String("Comanda noua: "+newOrder.toString() + "\nContinut: \n" + l.toString())+"\n");

            Serializator ser = new Serializator(); ser.serialization(this, "file.txt");

            assert listOfOrders.size() == oldSize + 1 : "Dimensiunea listei de comenzi nu e corecta!";
            return true;
        }
        assert listOfOrders.size() == oldSize : "Dimensiunea listei de comenzi nu e corecta!"; assert isWellFormed();
        return false;
    }



    @Override
    public boolean generateTimeIntervalReport(LocalTime t1, LocalTime t2) {
        assert t1 != null && t2 != null : "Parametri de intrare invalizi!"; assert isWellFormed();

        FileWriter fW = FileWriterClass.createFileWriter();
        Map<Order, List<MenuItem>> lOrdersAndItems = listOfOrders.entrySet().stream()
                        .filter(m -> { LocalTime t = LocalTime.of(m.getKey().getOrderDate().getHour(), m.getKey().getOrderDate().getMinute());
                        if(t1.getHour() > t2.getHour()){ return (t.minusHours(t2.getHour() + 1).isAfter(t1.minusHours(t2.getHour() + 1))
                        && t.minusHours(t2.getHour() + 1).isBefore(t2.minusHours(t2.getHour() + 1)));}
                        else{return (t.isAfter(t1) && t.isBefore(t2));}})
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        assert lOrdersAndItems != null : "Parametru de iesire e invalid!"; assert isWellFormed();
        return FileWriterClass.writeToFile(fW, lOrdersAndItems);
    }



    @Override
    public boolean generateSpecifiedTimesReport(int noTimes) {
        assert isWellFormed();

        FileWriter fW = FileWriterClass.createFileWriter();
        List<MenuItem> products = listOfOrders.entrySet().stream()
                .map(Map.Entry::getValue).reduce(new ArrayList<MenuItem>(), (l1, l2) -> {l1.addAll(l2); return l1;});

        List<MenuItem> productsOrderNoTimes = products.stream().filter(s -> (products.stream().filter(p -> p.equals(s)).count()) >= noTimes).collect(Collectors.toList());;

        assert productsOrderNoTimes != null : "Parametru de iesire invalid!"; assert isWellFormed();
        return FileWriterClass.writeProductsToFile(productsOrderNoTimes, fW);
    }

    public long NoApClient(int idClient, int amount){
        return listOfOrders.entrySet().stream().filter(o -> (o.getKey().getClientID() == idClient)).count();
    }

    public int orderPrice(List<MenuItem> m){
        return m.stream().mapToInt(MenuItem::computePrice).reduce(0, (o1, o2) -> o1 + o2);
    }

    @Override
    public boolean generateSpecifiedTimesPerClientReport(int specifiedNo, int amount) {
        assert isWellFormed();

        FileWriter fW = FileWriterClass.createFileWriter();
        Map<Order, List<MenuItem>> clientOrders = listOfOrders.entrySet().stream()
                .filter(o -> NoApClient(o.getKey().clientID, amount) >= specifiedNo && orderPrice(o.getValue()) >= amount)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        assert clientOrders != null : "Parametru de iesire invalid!"; assert isWellFormed();
        return FileWriterClass.writeToFile(fW, clientOrders);
    }

    @Override
    public boolean generateSpecifiedDateReport(LocalDate date) {
        assert date != null : "Nu exista comenzi!"; assert isWellFormed();

        FileWriter fW = FileWriterClass.createFileWriter();
        List<MenuItem> products = listOfOrders.entrySet().stream()
                                 .filter(m -> m.getKey().getOrderDate().getYear() == date.getYear()
                                 && m.getKey().getOrderDate().getMonthValue() == date.getMonthValue()
                                 && m.getKey().getOrderDate().getDayOfMonth() == date.getDayOfMonth())
                                 .map(Map.Entry::getValue).reduce(new ArrayList<MenuItem>(), (l1, l2) -> {l1.addAll(l2); return l1;});

        assert products != null : "Parametru de iesire invalid!"; assert isWellFormed();
        return FileWriterClass.writeProductsToFile(products, fW);
    }
}
