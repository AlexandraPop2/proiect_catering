package businessLayer;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Vector;

public interface IDeliveryServiceProcessing {

    /**
     * Importa BaseProducts din fisierul dat ca parametru in lista menu
     * <pre>
     *     <b>Preconditions: </b>  menu.size() >= 0
     * </pre>
     * <post>
     *     <b>Postconditions: </b> menu.size() >= old menu.size()
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param fileName Un nume de fisier
     * @return Daca import-ul s-a efectuat cu succes - true, altfel - false
     */
    boolean importItems(String fileName);

    /**
     * Adauga un BaseProduct in lista menu
     * <pre>
     *     <b>Preconditions: </b>  title != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> if true then menu.size() == (old menu.size() + 1)
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param title Titlul unui produs
     * @param rating Rating-ul unui produs
     * @param calories Caloriile unui produs
     * @param fat Grasimile unui produs
     * @param sodium Sodiul unui produs
     * @param protein Proteinele unui produs
     * @param price Pretul unui produs
     * @return True - daca s-a reusit adaugarea, altfel - false
     *
     */
    boolean addProduct(String title, double rating, int calories, int fat, int sodium, int protein, int price);

    /**
     * Sterge un produs din lista menu
     * <pre>
     *     <b>Preconditions: </b>  title != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> if true then menu.size() == (old menu.size() - 1) else menu.size() == old menu.size()
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param title Titlul produsului de sters
     * @return True - daca produsul exista si s-a sters cu succes, altfel - false
     */
    boolean deleteProduct(String title);

    /**
     * Modifica unele variabile instanta ale produsului ce are titlul title
     * <pre>
     *     <b>Preconditions: </b>  title != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> menu.size() == old menu.size()
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param title Titlul produsului de modificat
     * @param calories Caloriile noi
     * @param fat Grasimile noi
     * @param sodium Sodiul nou
     * @param price Pretul nou
     * @param protein Proteinele noi
     * @return True - daca exista produsul si s-a modificat cu succes, altfel - false
     */
    boolean modifyProduct(String title, int calories, int fat, int sodium, int price, int protein);

    /**
     * Creaza un produs compus ce contine produsele din lista l si are titlul title.
     * <pre>
     *     <b>Preconditions: </b> title != null && l != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> if true then menu.size() == (old menu.size() + 1) else menu.size() == old menu.size()
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param l Lista de produse
     * @param title Titlul noului produs
     * @return True - daca s-a creat produsul cu succes, altfel - false
     */
    boolean createCompositeProduct(List<BaseProduct> l, String title);

    /**
     * Formeaza vectori ce contin lista menu.
     * <pre>
     *     <b>Preconditions: </b> menuM != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> return != null
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param menuM Meniul dupa care sa se faca un vector
     * @return Un vector ce contine toate podusele din lista menu.
     */
    Vector viewProducts(List<MenuItem> menuM);

    /**
     * Cauta produsele dupa diverse criterii(titlu, calorii, proteine, sodiu, grasimi, rating, pret)
     * <pre>
     *     <b>Preconditions: </b> criteria != null && criteria.size == 7
     * </pre>
     * <post>
     *     <b>Postconditions: </b> return != null
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param criteria Lista de criterii dupa care se cauta produsul
     * @return O lista cu produsele care se incadreaza in criteriile dupa care au fost cautate din lista menu
     */
    List<MenuItem> searchForProduct(List criteria);

    /**
     * Creaza o noua comanda in lista de comenzi pentru un anumit client dat ca parametru
     * <pre>
     *     <b>Preconditions: </b> l != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> if true then listOfOrders.size() == (old listOfOrders.size() + 1) else listOfOrders.size() == old listOfOrders.size()
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param l Lista de produse de comandat
     * @param idClient Id-ul clientului ce plaseaza comanda
     * @return True - daca s-a realizat comanda cu succes, altfel - false
     */
    boolean createNewOrder(List<MenuItem> l, int idClient);

    /**
     * Genereaza un report baza pe comenzile ce se incadreaza intr-un interval orar
     * <pre>
     *     <b>Preconditions: </b> t1 != null && t2 != null
     * </pre>
     * <post>
     *     <b>Postconditions: </b> lOrdersAndItems != null
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param t1 Timpul de start
     * @param t2 Timpul de sfarsit
     * @return True - daca s-a putut genera report-ul, altfel - false
     */
    boolean generateTimeIntervalReport(LocalTime t1, LocalTime t2);

    /**
     * Genereaa un report cu produsele care au fost comandate de un anumit numar de ori
     * <pre>
     *     <b>Preconditions: </b> none
     * </pre>
     * <post>
     *     <b>Postconditions: </b> productsOrderNoTimes != null
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param noTimes Numarul de comenzi ale unui produs
     * @return True - daca s-a reusit generarea, altfel - false
     */
    boolean generateSpecifiedTimesReport(int noTimes);

    /**
     * Genereaza un report cu clientii care au comandat de minim un anumit numar de ori si valoare
     * <pre>
     *     <b>Preconditions: </b> none
     * </pre>
     * <post>
     *     <b>Postconditions: </b> clientOrders != null
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param specifiedNo Numarul de comenzi realizate de clienti
     * @param amount Pretul acelor comenzi
     * @return True - daca s-a reusit genereare, altfel - false
     */
    boolean generateSpecifiedTimesPerClientReport(int specifiedNo, int amount);

    /**
     * Genereaza un report cu comenzile realizate la anumita data
     * <pre>
     * <b>Preconditions: </b> date != null
     * </pre>
     * <post>
     * <b>Postconditions: </b>  products != null
     * </post>
     * <invariant>
     *    isWellFormed()
     * </invariant>
     * @param date Data la care sa fie facute comenzile
     * @return True - daca s-a reusit generarea report-ului, altfel -false
     */
    boolean generateSpecifiedDateReport(LocalDate date);
}
