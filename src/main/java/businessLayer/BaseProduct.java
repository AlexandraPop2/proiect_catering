package businessLayer;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class BaseProduct extends MenuItem{
    private String title;
    private double rating;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;
    private int price;

    public int computePrice(){
        return price;
    }

    @Override
    public int computeCalories() {
        return calories;
    }

    @Override
    public int computeProtein() {
        return protein;
    }

    @Override
    public int computeFat() {
        return fat;
    }

    @Override
    public int computeSodium() {
        return sodium;
    }

    @Override
    public double computeRatings() {
        return rating;
    }

    public void setCalories(int calories){ this.calories = calories; }

    public void setProtein(int protein){ this.protein = protein; }

    public void setFat(int fat){ this.fat = fat; }

    public void setSodium(int sodium){ this.sodium = sodium; }

    public void setPrice(int price){ this.price = price; }

    public BaseProduct(String title, double rating, int calories, int protein, int fat, int sodium, int price){
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;
        this.price = price;
    }

    public static BaseProduct createProduct(String line){
        Pattern p = Pattern.compile(",");
        List<String> l = null;

        l = Arrays.asList(p.split(line).clone());
        BaseProduct b = null;
        if(l.size() == 7){
            try{
                b = new BaseProduct(l.get(0), Double.parseDouble(l.get(1)), Integer.parseInt(l.get(2)), Integer.parseInt(l.get(3)),
                        Integer.parseInt(l.get(4)), Integer.parseInt(l.get(5)), Integer.parseInt(l.get(6)));
            }catch(NumberFormatException e) {
                System.out.println("Nu se poate crea obiectul!");
            }
        }
        return b;
    }

    public static BaseProduct menuItemToBaseProduct(MenuItem m){
        if(m instanceof BaseProduct){
            return (BaseProduct) m;
        }
        return null;
    }

    public String getTitle(){
        return title;
    }
    public void setTitle(String title){ this.title = title; }

    @Override
    public int hashCode(){
        return title.hashCode();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof BaseProduct && (title.equals(((BaseProduct) o).getTitle()) == true)){
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return this.title+", "+this.rating+", "+this.calories+", "+this.protein+", "+ this.fat+", "+this.sodium+", "+this.price;
    }


}
