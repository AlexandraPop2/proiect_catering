package businessLayer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Order implements Serializable {
    int orderID;
    int clientID;
    LocalDateTime orderDate;

    public Order(int orderID, int clientID, LocalDateTime date){
        this.orderID = orderID;
        this.clientID = clientID;
        this.orderDate = date;
    }

    public int getOrderID(){ return orderID; }
    public int getClientID() { return clientID; }
    public LocalDateTime getOrderDate(){ return orderDate; }

    @Override
    public int hashCode(){
        return (orderID + clientID) * orderDate.hashCode();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Order){
            return (orderID == ((Order) o).getOrderID() && clientID == ((Order) o).getClientID() && ((Order) o).getOrderDate().equals(orderDate));
        }
        return false;
    }

    @Override
    public String toString(){
        return orderID+", "+clientID+", "+orderDate;
    }
}
