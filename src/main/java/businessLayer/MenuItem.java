package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable{
    public abstract int computePrice();
    public abstract int computeCalories();
    public abstract int computeProtein();
    public abstract int computeFat();
    public abstract int computeSodium();
    public abstract double computeRatings();
    public abstract String getTitle();
}
