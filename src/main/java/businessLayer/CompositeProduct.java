package businessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem{
    private List<MenuItem> contents = new ArrayList<MenuItem>();
    private String title;

    public int computePrice(){
        int price = 0;
        for(MenuItem m: contents){
            price += m.computePrice();
        }
        return price;
    }

    @Override
    public int computeCalories() {
        int calories = 0;
        for(MenuItem m: contents){
            calories += m.computeCalories();
        }
        return calories;
    }

    @Override
    public int computeProtein() {
        int protein = 0;
        for(MenuItem m: contents){
            protein += m.computeProtein();
        }
        return protein;
    }

    @Override
    public int computeFat() {
        int fat = 0;
        for(MenuItem m: contents){
            fat += m.computeFat();
        }
        return fat;
    }

    @Override
    public int computeSodium() {
        int sodium = 0;
        for(MenuItem m: contents){
            sodium += m.computeProtein();
        }
        return sodium;
    }

    @Override
    public double computeRatings() {
        double rating = 0;
        int i = 0;
        for(MenuItem m: contents){
            rating += m.computeRatings();
            i++;
        }
        rating = rating / i;
        return rating;
    }

    ;
    public List<MenuItem> getList(){
        return contents;
    }
    public void setTitle(String title){ this.title = title; }
    public String getTitle(){ return title; }

    public static CompositeProduct menuItemToCompositeProduct(MenuItem m){
        if(m instanceof CompositeProduct){
            return (CompositeProduct) m;
        }
        return null;
    }

    @Override
    public int hashCode(){
        return title.hashCode();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof CompositeProduct && (title.equals(((CompositeProduct) o).getTitle()) == true)){
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return this.title+": "+this.getList();
    }
}
