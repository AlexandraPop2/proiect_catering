package businessLayer;

import dataLayer.Serializator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Accounts implements Serializable {
    private List<String> accounts = new ArrayList<String>();
    private List<String> accountsPass = new ArrayList<String>();
    private List<Integer> accountsId = new ArrayList<Integer>();

    public boolean addAccount(String username, String password, int idAccount){
        if(!accounts.contains(username)){
            accounts.add(username);
            accountsPass.add(password);
            accountsId.add(idAccount);
            Serializator ser = new Serializator();
            ser.serialization(this, "accounts.txt");
            return true;
        }
        return false;
    }

    public boolean accountMatching(String username, String password){
        if(accounts.contains(username)){
            int index = accounts.indexOf(username);
            if(accountsPass.indexOf(password) == index){
                return true;
            }
        }
        return false;
    }

    public int getNbOfAccount(){
        return accounts.size();
    }

    public int getAccountId(String username){
        return accountsId.get(accounts.indexOf(username));
    }

}
