import businessLayer.Accounts;
import businessLayer.DeliveryService;
import dataLayer.Serializator;
import presentationLayer.AdminController;
import presentationLayer.LogInFrame;
import presentationLayer.UIController;

public class Main {

   public static void main(String args[]){
       LogInFrame view = new LogInFrame();
       Serializator ser = new Serializator();

       Object o = ser.deserialization("accounts.txt");
       Accounts ac = o instanceof Accounts ? (Accounts)o : new Accounts() ;

       o = ser.deserialization("file.txt");
       DeliveryService del = o instanceof DeliveryService ? (DeliveryService) o : new DeliveryService();

       UIController controller = new UIController(view, ac, del);
       AdminController adminController = new AdminController(del, view);
       view.setVisible(true);
   }

}
